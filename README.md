# 电视TV直播

#### 介绍
简单的电视直播离线版，包括CCTV、卫视、地方台等频道，仅供学习交流使用。基于Android Jetpack Media3 原生组件实现直播播流，兼容性好，播放不卡顿。

#### 播放器组件使用说明
[安卓官网 Jetpack Media3 简介](https://android-dot-google-developers.gonglchuangl.net/media/media3?hl=th)

#### 自定义节目源
修改 \tv\app\src\main\res\raw\tv.txt 下的内容，重新打包即可（体验包在根目录下 app-debug.apk 文件）    
内容格式为：频道名称,源地址,图标（可选，对应raw目录下的图片名称）    
1）带图标：CCTV-1 综合,https://demo.m3u8play.com/m3u8/out/demo.m3u8,cctv1  
2）不带图标：CCTV-1 综合,https://demo.m3u8play.com/m3u8/out/demo.m3u8  
![自定义直播源内容格式](show.jpg)

#### 预览图
 >操作说明:  
 > 1）点击屏幕右侧或遥控器的确认键，可控制频道列表显示或隐藏  
 > 2）点击左侧**央视、卫视**按钮，可切换频道列表  
 > 3）点击对应的频道播放  
 > 4）遥控器的上键（频道+），下键（频道-），左键（切换央视），右键（切换卫视）  
 > 5）在屏幕**左下角**上滑（频道+），下滑（频道-），左滑（切换央视），右滑（切换卫视）  

![频道](show1.jpg)
![频道](show2.jpg)
![播放](show3.jpg)


