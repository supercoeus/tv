package com.example.tv.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tv.R;
import com.example.tv.entity.TvEntity;

import java.io.IOException;
import java.util.List;

public class TvAdapter extends RecyclerView.Adapter<TvAdapter.ViewHolder> {

    private List<TvEntity> localDataSet;

    Context mContext;

    ItemOnClickListener itemOnClickListener;

    public void setLocalDataSet(List<TvEntity> localDataSet) {
        this.localDataSet = localDataSet;
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;
        private final ImageView imageView;
        private final LinearLayout linearLayout;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            textView =  view.findViewById(R.id.textView);
            imageView =  view.findViewById(R.id.iv);
            linearLayout =  view.findViewById(R.id.ll);

        }

        public ImageView getImageView() {
            return imageView;
        }

        public LinearLayout getLinearLayout() {
            return linearLayout;
        }

        public TextView getTextView() {
            return textView;
        }
    }

    /**
     * Initialize the dataset of the Adapter
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView
     */
    public TvAdapter(Context context,List<TvEntity> dataSet, ItemOnClickListener listener) {
        mContext = context;
        localDataSet = dataSet;
        itemOnClickListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(viewType == 0?R.layout.list_item_cctv:R.layout.list_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        TvEntity tvEntity = localDataSet.get(position);
        if(tvEntity.getTitle().startsWith("CCTV")){
            return 0;
        }
        return 1;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        TvEntity tvEntity = localDataSet.get(position);
        TextView textView = viewHolder.getTextView();
        ImageView imageView = viewHolder.getImageView();
        LinearLayout linearLayout = viewHolder.getLinearLayout();
        if(tvEntity.getIcon() != null){
            Glide.with(mContext)
                    .load("android.resource://com.example.tv/raw/" + tvEntity.getIcon())
                    .into(imageView);
        }
        textView.setText(tvEntity.getTitle());
        linearLayout.setOnClickListener(v -> {
            try {
                // 如果当前位置和之前选中的位置不同，则更新选中位置，并刷新RecyclerView
                itemOnClickListener.itemClick(tvEntity,position);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }

    public interface ItemOnClickListener{
        void itemClick(TvEntity tv,int position) throws IOException;
    }
}
